package com.honglinxiong.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HlxGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(HlxGatewayApplication.class, args);
    }

}
