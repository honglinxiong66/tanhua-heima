package com.honglinxiong.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HlxBaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(HlxBaseApplication.class, args);
    }

}
