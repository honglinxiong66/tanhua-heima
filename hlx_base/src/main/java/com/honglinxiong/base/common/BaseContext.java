package com.honglinxiong.base.common;

/**
 * @Author HongLinXiong
 * @Date 2023/1/29 16:18
 */
public class BaseContext {
    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static void setCurrentId(String id) {
        threadLocal.set(id);
    }

    public static String getCurrentId() {
        return threadLocal.get();
    }
}