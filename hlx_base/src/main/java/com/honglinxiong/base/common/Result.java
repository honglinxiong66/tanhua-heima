package com.honglinxiong.base.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author HongLinXiong
 * @Date 2023/1/29 16:21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {
    private int code;
    private String message;
    private T data;

    public static <T> Result<T> success(T t) {
        return success(200, "成功", t);
    }

    public static <T> Result<T> success(int code, String message, T t) {
        Result<T> result = new Result<>();
        result.code = code;
        result.message = message;
        result.data = t;
        return result;
    }

    public static <T> Result<T> error(String message) {
        return error(-1, message, null);
    }


    public static <T> Result<T> error(int code, String message, T t) {
        Result<T> result = new Result<>();
        result.code = code;
        result.message = message;
        result.data = t;
        return result;
    }
}
