package com.honglinxiong.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.honglinxiong.user.entity.po.ThUsers;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author honglinxiong
 * @since 2023-01-29
 */
@Mapper
public interface ThUsersMapper extends BaseMapper<ThUsers> {

}
