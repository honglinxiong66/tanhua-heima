package com.honglinxiong.user.exception;

/**
 * @Author HongLinXiong
 * @Date 2023/1/29 16:24
 */
public class BusinessException extends RuntimeException{
    public BusinessException() {
    }

    public BusinessException(String message) {
        super(message);
    }
}
