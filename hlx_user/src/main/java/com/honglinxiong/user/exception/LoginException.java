package com.honglinxiong.user.exception;

/**
 * @Author HongLinXiong
 * @Date 2023/1/29 16:24
 */
public class LoginException extends BusinessException {
    public LoginException() {
    }

    public LoginException(String message) {
        super(message);
    }
}