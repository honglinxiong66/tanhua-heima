package com.honglinxiong.user.entity.dto;

import lombok.Data;

/**
 * @Author HongLinXiong
 * @Date 2023/1/29 19:09
 */
@Data
public class LoginDTO {
    private String phone;
    private String code;
}