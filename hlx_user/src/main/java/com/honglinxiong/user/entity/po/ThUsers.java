package com.honglinxiong.user.entity.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Timestamp;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author honglinxiong
 * @since 2023-01-29
 */
@Getter
@Setter
@TableName("th_users")
public class ThUsers implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户手机号
     */
    private String phone;

    /**
     * 0-女 1-男
     */
    private Boolean gender;

    /**
     * 用户昵称
     */
    private String userName;

    /**
     * 生日
     */
    private String birthday;

    /**
     * 城市
     */
    private String city;

    /**
     * 创建时间
     */
    private Timestamp created;

    /**
     * 最后更新时间
     */
    private Timestamp updated;

    /**
     * 是否软删除 0-没有删除 1-已删除
     */
    private String deleted;
}
