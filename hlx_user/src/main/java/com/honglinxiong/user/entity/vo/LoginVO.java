package com.honglinxiong.user.entity.vo;

import lombok.Data;

/**
 * @Author HongLinXiong
 * @Date 2023/1/29 19:08
 */
@Data
public class LoginVO {
    private String token;
}