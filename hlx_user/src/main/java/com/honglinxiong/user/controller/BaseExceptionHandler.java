package com.honglinxiong.user.controller;

import com.honglinxiong.base.common.Result;
import com.honglinxiong.user.exception.BusinessException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author HongLinXiong
 * @Date 2023/1/29 16:33
 */
@ControllerAdvice
public class BaseExceptionHandler {
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result<Object> error(Exception e) {
        return Result.error("异常错误");
    }

    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public Result<Object> error(BusinessException e) {
        return Result.error(e.getMessage());
    }

}