package com.honglinxiong.user.controller;


import com.honglinxiong.base.common.Result;
import com.honglinxiong.user.entity.po.ThUsers;
import com.honglinxiong.user.entity.dto.LoginDTO;
import com.honglinxiong.user.service.impl.ThUsersServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author honglinxiong
 * @since 2023-01-29
 */
@RestController
@RequestMapping("/thUsers")
public class ThUsersController {
    @Autowired
    private ThUsersServiceImpl thUsersServiceImpl;

    @PostMapping("sendMessage")
    public Result sendMessage(@RequestBody LoginDTO loginDTO){
        return thUsersServiceImpl.sendMessage(loginDTO);
    }

    @PostMapping("login")
    public Result login(@RequestBody LoginDTO loginDTO){
        return thUsersServiceImpl.login(loginDTO);
    }

    @PutMapping("info")
    public Result info(@RequestBody ThUsers thUsers){
        return thUsersServiceImpl.info(thUsers);
    }
}