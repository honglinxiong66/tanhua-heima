package com.honglinxiong.user.service.impl;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.jwt.JWT;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.honglinxiong.base.common.BaseContext;
import com.honglinxiong.base.common.Result;
import com.honglinxiong.user.entity.po.ThUsers;
import com.honglinxiong.user.entity.dto.LoginDTO;
import com.honglinxiong.user.entity.vo.LoginVO;
import com.honglinxiong.user.mapper.ThUsersMapper;
import com.honglinxiong.user.service.ThUsersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author honglinxiong
 * @since 2023-01-29
 */
@Slf4j
@Service
public class ThUsersServiceImpl extends ServiceImpl<ThUsersMapper, ThUsers> implements ThUsersService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private ThUsersService thUsersService;


    String key = "1234";

    public Result sendMessage(LoginDTO loginDTO) {
        String code = RandomUtil.randomNumbers(6);
        log.info("{}", code);
        redisTemplate.opsForValue().set("sms" + loginDTO.getPhone(), code, 180, TimeUnit.SECONDS);
        return Result.success("短信发送成功");
    }

    public Result login(LoginDTO loginDTO) {
        String code = loginDTO.getCode();
        String redisCode = (String) redisTemplate.opsForValue().get("sms" + loginDTO.getPhone());
        if (!code.equals(redisCode)) {
            return Result.error("验证码错误");
        }
        LambdaQueryWrapper<ThUsers> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ThUsers::getPhone, loginDTO.getPhone());
        ThUsers one = thUsersService.getOne(wrapper);
        if (ObjectUtil.isEmpty(one)) {
            ThUsers thUsers = new ThUsers();
            thUsers.setPhone(loginDTO.getPhone());
            thUsersService.save(thUsers);
            String token = JWT.create()
                    .setPayload("id", thUsers.getUserId())
                    .setKey(key.getBytes())
                    .sign();
            log.info(token);
            redisTemplate.opsForValue().set(token, token, 3600, TimeUnit.SECONDS);
            LoginVO tokenVo = new LoginVO();
            tokenVo.setToken(token);
            tokenVo.setFirstLogin(true);
            return Result.success(tokenVo);
        }
        String token = JWT.create()
                .setPayload("id", one.getUserId())
                .setKey(key.getBytes())
                .sign();
        log.info(token);
        redisTemplate.opsForValue().set(token, token, 3600, TimeUnit.SECONDS);
        LoginVO tokenVo = new LoginVO();
        tokenVo.setToken(token);
        return Result.success(tokenVo);
    }

    public Result info(ThUsers thUsers){
        String id = BaseContext.getCurrentId();
        thUsers.setUserId(id);
        thUsersService.updateById(thUsers);
        return Result.success(null);
    }
}