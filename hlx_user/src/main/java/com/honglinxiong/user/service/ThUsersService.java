package com.honglinxiong.user.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.honglinxiong.user.entity.po.ThUsers;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author honglinxiong
 * @since 2023-01-29
 */
public interface ThUsersService extends IService<ThUsers> {

}
