package com.honglinxiong.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HlxUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(HlxUserApplication.class, args);
    }

}
