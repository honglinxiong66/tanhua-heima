drop database if exists `hlx_tanhua`;
create database `hlx_tanhua` charset utf8mb4;
use `hlx_tanhua`;

create table `th_users`
(
    `user_id` varchar(64) not null default '' comment '用户id',
    `phone` varchar(20) not null default '' unique comment '用户手机号',
    `gender` tinyint(1) not null default 0 comment '0-女 1-男',
    `user_name` varchar(30) not null default '' comment '用户昵称',
    `birthday` varchar(10) not null default '' comment '生日',
    `city` varchar(50) not null default '' comment '城市',
    `created`     datetime     not null default CURRENT_TIMESTAMP comment '创建时间',
    `updated`     datetime     null comment '最后更新时间',
    `deleted`     char(1)      not null default '0' comment '是否软删除 0-没有删除 1-已删除',
    primary key (`user_id`)
)comment '用户表'